from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score, roc_curve, auc
import pandas as pd
import joblib


data = pd.read_csv('train.csv')
target = data['target']
input_X = data\
    .drop('target', 1)\
    .drop('ID_code', 1)
print(data.head(60))
print(target.head(6))
print(input_X.head(60))


model = LogisticRegression(class_weight='auto', max_iter=9000000)
cl = model.fit(input_X, target)
joblib.dump(cl, 'LogisticRegression.pkl')

predicted_target = cl.predict(input_X)
false_positive_rate, true_positive_rate, thresholds = roc_curve(target, predicted_target)
roc_auc = auc(false_positive_rate, true_positive_rate)


print(false_positive_rate, true_positive_rate)
print(roc_auc)
print(roc_auc_score(target, predicted_target))
