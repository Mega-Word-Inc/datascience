import text_tokenizer2 as tt
import os
from sys import argv


path = os.getcwd()
input_path = path + '\\input'
output_path = path + '\\output_1'

if len(argv) >= 3:
    input_path = argv[1]
    output_path = argv[2]

files = os.listdir(input_path)

if not os.path.isdir(output_path):
    os.mkdir(output_path)

for filename in files:
    print(input_path + '\\' + filename)
    input_file = open(input_path + '\\' + filename,
                      encoding='utf-8')
    output_file = open(output_path + '\\' + filename,
                       mode='w', encoding='utf-8')
    for line in input_file:
        print(line)
        # output_file.write(' '.join(tt.tokenize(line)))
        mass = tt.tokenize(line)
        print(mass)
        output_file.write(' '.join(mass))
    input_file.close()
    output_file.close()
