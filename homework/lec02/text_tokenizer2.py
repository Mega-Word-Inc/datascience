import re

CONST_TOKENIZE_REGEX_PATTERN = re.compile(r'\.+|,+|:+|;+|\-+'
                                          r'|[-+]?\d+\.\d*'
                                          r'|\d+[:]+\d+(?!\w)*'
                                          r'|\d+(?:(?:\w\b)+)'
                                          r'|[A-Z]+[-_a-z]*[-_A-Z]*[-_a-z]*'
                                          r'|[a-z]+[-_a-z]*'
                                          r'|[А-Я]+[-_а-я]*[-_А-Я]*[-_а-я]*'
                                          r'|[а-я]+[-_а-я]*'
                                          r'|(?:(?!\d)\w)+[\-]*(?:(?!\d)\w)+'
                                          r'|\d+|\w+|(?:(?!\d\w))\S(?:(?!\d\w))'
                                          r'|\\p{L}+')


def tokenize(string):
    return CONST_TOKENIZE_REGEX_PATTERN.findall(string)
