import text_tokenizer2 as tt
import os
import re
from sys import argv


CONST_DIGITS_PATTERN = re.compile(r'\d+\.?\d*',
                                  flags=re.IGNORECASE)
path = os.getcwd()
input_path = path + '\\input'
output_path = path + '\\output_2'

if len(argv) >= 3:
    input_path = argv[1]
    output_path = argv[2]

files = os.listdir(input_path)

if not os.path.isdir(output_path):
    os.mkdir(output_path)

for filename in files:
    print(input_path + '\\' + filename)
    input_file = open(input_path + '\\' + filename,
                      encoding='utf-8')
    output_file = open(output_path + '\\' + filename,
                       mode='w', encoding='utf-8')
    for line in input_file:
        indexes = []
        tokens = tt.tokenize(line)
        print(line)
        print(tokens)
        for i in range(len(tokens)):
            match = CONST_DIGITS_PATTERN.fullmatch(tokens[i])
            if CONST_DIGITS_PATTERN.fullmatch(tokens[i]):
                print(str(i), ":", tokens[i])
                indexes.append(str(i))
        # print(indexes)
        output_file.write(', '.join(indexes))
    input_file.close()
    output_file.close()
