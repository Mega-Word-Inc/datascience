import text_tokenizer2 as tt
import os
import re
from sys import argv

CONST_MONTH_NAME_PATTERN = re.compile(r'декабр[ьея]|январ[ьея]|феврал[ьея]'
                                      r'|март[а|е]?|апрел[ьея]|ма[йея]'
                                      r'|ию[нл][ьея]|август[еа]?'
                                      r'|сентябр[ьея]|октябр[ьея]|ноябр[ьея]',
                                      flags=re.IGNORECASE)
CONST_YEAR_NAME_PATTERN = re.compile(r'год.?\b')
CONST_DAY_OF_MONTH_PATTERN = re.compile(r'[1-9]|0[1-9]|[12][0-9]|3[01]')
CONST_YEAR_PATTERN = re.compile(r'\d+')

path = os.getcwd()
input_path = path + '\\input'
output_path = path + '\\output_4'

if len(argv) >= 3:
    input_path = argv[1]
    output_path = argv[2]

files = os.listdir(input_path)

if not os.path.isdir(output_path):
    os.mkdir(output_path)

for filename in files:
    print(input_path + '\\' + filename)
    input_file = open(input_path + '\\' + filename,
                      encoding='utf-8')
    output_file = open(output_path + '\\' + filename,
                       mode='w', encoding='utf-8')
    for line in input_file:
        indexes = []
        tokens = tt.tokenize(line)
        print(line)
        print(tokens)
        tokens_len = len(tokens)
        if tokens_len > 0:
            for i in range(tokens_len-1):
                if (CONST_DAY_OF_MONTH_PATTERN.fullmatch(tokens[i]) and
                    CONST_MONTH_NAME_PATTERN.fullmatch(tokens[i + 1])) \
                        or (CONST_YEAR_PATTERN.fullmatch(tokens[i]) and
                            CONST_YEAR_NAME_PATTERN.fullmatch(tokens[i + 1])):
                    print(str(i), ':', tokens[i])
                    print(str(i+1), ':', tokens[i+1])
                    indexes.append(str(i))
            # print(indexes)
            output_file.write(', '.join(indexes))
    input_file.close()
    output_file.close()
