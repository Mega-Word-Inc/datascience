import text_tokenizer2 as tt
import os
import re
from sys import argv

CONST_DIGITS_PATTERN = re.compile(r'\d{1,2}|100')

CONST_DIGITS_SIMPLE = [
    'нол',
    'один',
    'дв',
    'три',
    'четыр',
    'пят',
    'шест',
    'сем',
    'восем',
    'девят'
]

CONST_DIGIT_SUFFIX = [
    ['ь', ''],
    ['', 'надцать'],
    ['а', 'надцать'],
    ['', 'надцать'],
    ['е', 'надцать'],
    ['ь', 'надцать'],
    ['ь', 'надцать'],
    ['ь', 'надцать'],
    ['ь', 'надцать'],
    ['ь', 'надцать'],
]

CONST_DIGITS_RANK = [
    'ноль',
    'десять',
    'двадцать',
    'тридцать',
    'сорок',
    'пятьдесят',
    'шестьдесят',
    'семьдесят',
    'восемьдесят',
    'девяносто',
    'сто'
]


def normalize(tokens):
    normalized_tokens = []
    for i in range(len(tokens)):
        if CONST_DIGITS_PATTERN.fullmatch(tokens[i]):
            if len(tokens[i]) == 1 or \
                    (len(tokens[i]) == 2 and tokens[i][0] == '0'):
                normalized_tokens.append(
                    CONST_DIGITS_SIMPLE[int(tokens[i])] +
                    CONST_DIGIT_SUFFIX[int(tokens[i])][0]
                )
            elif len(tokens[i]) == 2:
                if tokens[i][1] == '0':
                    normalized_tokens.append(
                        CONST_DIGITS_RANK[int(tokens[i][0])]
                    )
                elif tokens[i][0] == '1':
                    normalized_tokens.append(
                        CONST_DIGITS_SIMPLE[int(tokens[i][1])] +
                        CONST_DIGIT_SUFFIX[int(tokens[i][0])][1]
                    )
                else:
                    normalized_tokens.append(
                        CONST_DIGITS_RANK[int(tokens[i][0])]
                    )
                    normalized_tokens.append(
                        CONST_DIGITS_SIMPLE[int(tokens[i][1])] +
                        CONST_DIGIT_SUFFIX[int(tokens[i][1])][0]
                    )
            elif int(tokens[i]) == 100:
                normalized_tokens.append(
                    CONST_DIGITS_RANK[-1]
                )
        else:
            normalized_tokens.append(tokens[i])
    return normalized_tokens


path = os.getcwd()
input_path = path + '\\input'
output_path = path + '\\output_5'

if len(argv) >= 3:
    input_path = argv[1]
    output_path = argv[2]

files = os.listdir(input_path)

if not os.path.isdir(output_path):
    os.mkdir(output_path)


for filename in files:
    print(input_path + '\\' + filename)
    input_file = open(input_path + '\\' + filename,
                      encoding='utf-8')
    output_file = open(output_path + '\\' + filename,
                       mode='w', encoding='utf-8')
    for line in input_file:
        indexes = []
        tokens = tt.tokenize(line)
        normalize_tokens = normalize(tokens)
        print(line)
        print(tokens)
        print(normalize_tokens)
        output_file.write(' '.join(normalize_tokens))
    input_file.close()
    output_file.close()
