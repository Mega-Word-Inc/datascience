import numbers
import vector


class Matrix:

    def __init__(self, matrix):
        if self.__check_matrix__(matrix):
            self.__matrix = matrix
        else:
            raise TypeError(
                "Матрица не квадратная " +
                "или содержит не численные типы"
            )

    def __str__(self):
        return self.__matrix.__str__()

    @property
    def matrix(self):
        return self.__matrix

    @matrix.setter
    def matrix(self, matrix):
        if self.__check_matrix__(matrix):
            self.__matrix = matrix
        else:
            raise TypeError(
                "Матрица не квадратная " +
                "или содержит не численные типы"
            )

    def __check_matrix__(self, matrix) -> bool:
        n = len(matrix)
        for i in range(n):
            if len(matrix[i]) != n:
                return False
            for j in range(n):
                if not isinstance(matrix[i][j], numbers.Number):
                    return False
        return True

    def plus(self, other):
        if isinstance(other, type(self)):
            if other.size == self.size:
                new_matrix = []
                for i in range(len(self.__matrix)):
                    new_column = []
                    for j in range(len(self.__matrix)):
                        new_column.append(
                            self.__matrix[i][j] + other.__matrix[i][j]
                        )
                    new_matrix.append(new_column)
                return Matrix(new_matrix)
            else:
                raise ValueError(
                    "Матрицы должны иметь одинаковую размерность"
                )
        elif isinstance(other, vector.Vector):
            if other.size == self.size:
                new_matrix = []
                for i in range(len(self.__matrix)):
                    new_column = []
                    for j in range(len(self.__matrix)):
                        new_column.append(
                            self.__matrix[i][j] + other.vector[i]
                        )
                    new_matrix.append(new_column)
                return Matrix(new_matrix)
            else:
                raise ValueError(
                    "Матрица и вектор должны иметь одинаковую размерность"
                )
        elif isinstance(other, numbers.Number):
            new_matrix = []
            for i in range(len(self.__matrix)):
                new_column = []
                for j in range(len(self.__matrix)):
                    new_column.append(
                        self.__matrix[i][j] + other
                    )
                new_matrix.append(new_column)
            return Matrix(new_matrix)
        else:
            raise TypeError(
                "Несовместимый тип для операции"
            )

    def minus(self, other):
        if isinstance(other, type(self)):
            new_matrix = []
            for i in range(len(other.__matrix)):
                new_column = []
                for j in range(len(other.__matrix[i])):
                    new_column.append(
                        other.__matrix[i][j] * (-1)
                    )
                new_matrix.append(new_column)
            return self.plus(new_matrix)
        elif isinstance(other, numbers.Number):
            return self.plus(
                other * (-1)
            )
        else:
            raise TypeError(
                "Несовместимый тип для операции"
            )

    def multiply(self, other):
        if isinstance(other, type(self)):
            if other.size == self.size:
                new_matrix = []
                for i in range(len(self.__matrix)):
                    new_column = []
                    for j in range(len(self.__matrix)):
                        elem = 0
                        for k in range(len(self.__matrix)):
                            elem += self.__matrix[i][k]*other.__matrix[k][j]
                        new_column.append(elem)
                    new_matrix.append(new_column)
                return Matrix(new_matrix)
            else:
                raise ValueError(
                    "Матрицы должны иметь одинаковую размерность"
                )
        elif isinstance(other, vector.Vector):
            if other.size == self.size:
                new_vector = []
                for i in range(len(self.__matrix)):
                    elem = 0
                    for j in range(len(self.__matrix)):
                        elem += self.__matrix[i][j] * other.vector[j]
                    new_vector.append(elem)
                return vector.Vector(new_vector)
            else:
                raise ValueError(
                    "Матрицы и вектор должны иметь одинаковую размерность"
                )
        elif isinstance(other, numbers.Number):
            new_matrix = []
            for i in range(len(self.__matrix)):
                new_column = []
                for j in range(len(self.__matrix[i])):
                    new_column.append(self.__matrix[i][j] * other)
                new_matrix.append(new_column)
            return Matrix(new_matrix)
        else:
            raise TypeError(
                "Несовместимый тип для операции"
            )

    def div(self, other):
        if isinstance(other, type(self)):
            if other.size == self.size:
                new_matrix = []
                for i in range(len(self.__matrix)):
                    new_column = []
                    for j in range(len(self.__matrix)):
                        other.__matrix[i][j] = 1 / other.__matrix[i][j]
                return self.muliply(other)
            else:
                raise ValueError(
                    "Матрицы должны иметь одинаковую размерность"
                )
        elif isinstance(other, vector.Vector):
            if other.size == self.size:
                new_vector = []
                for i in range(len(self.__matrix)):
                    other.vector[i] = 1 / other.vector[i]
                return self.plus(other)
            else:
                raise ValueError(
                    "Матрицы и вектор должны иметь одинаковую размерность"
                )
        elif isinstance(other, numbers.Number):
            new_matrix = []
            for i in range(len(self.__matrix)):
                new_column = []
                for j in range(len(self.__matrix[i])):
                    new_column.append(self.__matrix[i][j] * other)
                new_matrix.append(new_column)
            return Matrix(new_matrix)
        else:
            raise TypeError(
                "Несовместимый тип для операции"
            )

    def transpose(self):
        new_matrix = []
        for i in range(len(self.__matrix)):
            new_column = []
            for j in range(len(self.__matrix)):
                new_column.append(self.__matrix[j][i])
            new_matrix.append(new_column)
        return Matrix(new_matrix)

    @property
    def size(self):
        return len(self.__matrix)


if __name__ == '__main__':
    arr = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ]
    arr_one = [
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]
    ]
    noles = [
        [1, 3, 4],
        [0, 2, 1],
        [1, 5, -1]
    ]
    vector_mas = [1, 2, 3]
    test = Matrix(arr)
    one = Matrix(arr_one)
    vector_1 = vector.Vector(vector_mas)
    noles_m = Matrix(noles)
    print(noles_m)
    noles_m.__matrix = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]
    print(noles_m)
    print(test)
    print(test.plus(one))
    print(test.multiply(one))
    print(test.transpose())
    print(test.transpose().transpose())
    print(test.plus(vector_1))
    print(test.multiply(vector_1))
