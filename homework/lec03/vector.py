import numbers
import math
import matrix


class Vector:

    def __init__(self, vector):
        if self.__check_vector__(vector):
            self.__vector = vector
        else:
            TypeError(
                "Вектор пустой " +
                "или содержит не численные типы"
            )

    def __str__(self):
        return self.__vector.__str__()

    def __check_vector__(self, vector) -> bool:
        n = len(vector)
        for i in range(n):
            if not isinstance(vector[i], numbers.Number):
                return False
        return True

    def plus(self, other):
        if isinstance(other, type(self)):
            if other.size == self.size:
                new_vector = []
                for i in range(self.size):
                    new_vector.append(self.vector[i] + other.vector[i])
                return Vector(new_vector)
            else:
                raise ValueError("Векторы должны иметь одинаковую размерность")
        elif isinstance(other, matrix.Matrix):
            if other.size == self.size:
                new_vector = []
                for i in range(self.size):
                    new_value = 0
                    for j in range(other.size):
                        new_value += self.vector[j] + other.matrix[i][j]
                    new_vector.append(new_value)
                return Vector(new_vector)
            else:
                raise ValueError("Векторы должны иметь одинаковую размерность")
        elif isinstance(other, numbers.Number):
            new_vector = []
            for i in range(self.size):
                new_vector.append(self.vector[i] + other)
            return Vector(new_vector)
        else:
            raise TypeError("Несовместимый тип для операции")

    def minus(self, other):
        if isinstance(other, type(self)):
            new_vector = []
            for i in range(other.size):
                new_vector.append(other.vector[i] * (-1))
            return self.plus(new_vector)
        elif isinstance(other, matrix.Matrix):
            new_matrix = []
            for i in range(other.size):
                new_column = []
                for j in range(other.size):
                    new_column.append(other.matrix[i][j] * (-1))
                new_matrix.append(new_column)
            return self.plus(new_matrix)
        elif isinstance(other, numbers.Number):
            return self.plus(other * (-1))
        else:
            raise TypeError("Несовместимый тип для операции")

    def multiply(self, other):
        if isinstance(other, type(self)):
            if other.size == self.size:
                new_vector = []
                for i in range(other.size):
                    new_vector.append(self.vector[i] * other.vector[i])
                return Vector(new_vector)
        elif isinstance(other, matrix.Matrix):
            if other.size == self.size:
                new_vector = []
                for i in range(other.size):
                    elem = 0
                    for j in range(other.size):
                        elem += self.vector[j] * other.matrix[i][j]
                    new_vector.append(elem)
                return Vector(new_vector)
            else:
                raise ValueError(
                    "Матрицы и вектор должны иметь одинаковую размерность"
                )
        elif isinstance(other, numbers.Number):
            new_vector = []
            for i in range(self.size):
                new_vector.append(self.vector[i] * other)
            return Vector(new_vector)
        else:
            raise TypeError("Несовместимый тип для операции")

    @property
    def size(self):
        return len(self.__vector)

    @property
    def vector(self):
        return self.__vector

    @vector.setter
    def vector(self, vector):
        if self.__check_vector__(vector):
            self.__vector = vector
        else:
            TypeError(
                "Вектор пустой " +
                "или содержит не численные типы"
            )

    @property
    def norm(self):
        evklid = 0
        for i in range(self.size):
            evklid += self.vector[i] ** 2
        return math.sqrt(evklid)


if __name__ == '__main__':
    vector_1 = Vector([3, -12, 2, 4, 5])
    print(vector_1)
    print(vector_1.norm)
